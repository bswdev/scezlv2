package sce.zl

import org.holoeverywhere.app.Activity

abstract class BaseHoloActivity extends Activity {

//  override def onCreateOptionsMenu(menu: Menu) = {
//    getMenuInflater.inflate(R.menu.menu_options, menu)
//    true
//  }

//  override def onOptionsItemSelected(item: MenuItem) = item.getItemId match {
//    case R.id.opt_search => startActivity(new Intent(this, classOf[Query])); true
//    //case R.id.opt_manage => startActivity(new Intent(this, Manage.class));
//    //	return true;
//    case R.id.opt_bookmarks => startActivity(new Intent(this, classOf[Bookmarks])); true
//    //		case R.id.opt_history:
//    //			startActivity(new Intent(this, History.class));
//    //			return true;
//    //		case R.id.opt_settings:
//    //			startActivity(new Intent(this, Settings.class));
//    //			return true;
//    //		case R.id.opt_about:
//    //			showDialog(id);
//    //			return true;
//    case _ => super.onOptionsItemSelected(item)
//  }

  def setLayout(layout: Int) {
    setContentView(layout)
    init
  }

  // initialize widgets, et al
  def init: Unit

  def string = getString _
}