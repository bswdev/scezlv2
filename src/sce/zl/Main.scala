package sce.zl

import org.holoeverywhere.app.Activity

import com.actionbarsherlock.view._

import android.os.Bundle
import android.view.View
import sce.zl.db.Category
import sce.zl.traits._

class Main extends Activity
  with Databasable
  with Alertable
  with Toastable {

  override def onCreate(state: Bundle) {
    super.onCreate(state)

    setContentView(R.layout.activity_main)
  }

  def onControl(v: View) = v.getId match {
    case R.id.test => toastShort(R.string.hello_world)
    case _ => // do nothing
  }

  override def onCreateOptionsMenu(menu: Menu) = {
    getSupportMenuInflater.inflate(R.menu.activity_main, menu)
    super.onCreateOptionsMenu(menu)
  }

  override def onOptionsItemSelected(item: MenuItem) = item.getItemId match {
    case R.id.menu_settings => executeDb[Unit, Unit, java.util.List[Category]] { (helper, publish, params) =>
      val dao = helper.findDao(classOf[Category])
      dao.queryForAll
    } then { result =>
      toastShort("there are %d categories in all" format result.size)
    } execute()
    
    true

    case _ => super.onOptionsItemSelected(item)
  }
}