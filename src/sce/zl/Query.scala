package sce.zl

import java.net.URLEncoder
import java.text.MessageFormat
import java.util.concurrent.Callable
import java.util.Arrays
import scala.Array.canBuildFrom
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.JavaConversions.bufferAsJavaList
import org.holoeverywhere.app.AlertDialog
import org.holoeverywhere.widget.Button
import org.holoeverywhere.widget.CheckBox
import org.holoeverywhere.widget.EditText
import org.holoeverywhere.widget.LinearLayout
import org.holoeverywhere.widget.ListView
import org.holoeverywhere.widget.RadioButton
import org.holoeverywhere.widget.Spinner
import org.holoeverywhere.widget.TextView
import org.holoeverywhere.ArrayAdapter
import org.jsoup.nodes.Node
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.text.TextUtils
import android.view.animation.Animation.AnimationListener
import android.view.animation.Animation
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CompoundButton
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.RelativeLayout
import sce.zl.db.Area
import sce.zl.db.CheckedHood
import sce.zl.db.Filter
import sce.zl.db.Hood
import sce.zl.db.Location
import sce.zl.db.Region
import sce.zl.db.Search
import sce.zl.db.Section
import sce.zl.db.SubCategory
import sce.zl.db.Category
import sce.zl.db.DatabaseHelper
import sce.zl.db.Locale
import sce.zl.traits.Animatable
import sce.zl.traits.Databasable
import sce.zl.traits.Imageable
import sce.zl.traits.Parsable
import sce.zl.traits.Preferable
import sce.zl.traits.Toastable
import sce.zl.traits.Viewable
import sce.zl.widget.BaseSpinnerAdapter
import sce.zl.widget.OnSpinnerItemSelectedListener
import sce.zl.widget.ClearableEditText
import sce.zl.widget.EventSpinner
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.view.LayoutInflater
import android.content.Context

class Query extends BaseHoloActivity
  with Animatable
  with Databasable
  with Imageable // necessary for clearing files
  with Parsable
  with Preferable
  with Toastable
  with Viewable {

  //override val titleLayout = R.layout.title_query

  override def onCreate(state: Bundle) {
    super.onCreate(state)

    //setContentView(R.layout.act_query)
    setLayout(R.layout.act_query)

    // set title
    //setTitle(R.string.ttl_new_search)

    // init search
    fromPref(Query.prefSearch, -1) match {
      case -1 =>
        // load areas
        executeDb[Unit, Unit, List[Area]] { (helper, publish, params) =>
          val dao = helper.findDao(classOf[Region])
          helper.getList(classOf[Area], "region_id" -> dao.queryForFirst(dao.queryBuilder.prepare).id)
        } then { result =>
          adapterArea.setData(result) // set areas
          spArea.setSelection(0, true) // select first
        } otherwise { e =>
          error("failed to load areas for selected region", e)
        } execute ()

        // load categories
        executeDb[Unit, Unit, List[Category]] { (helper, publish, params) =>
          helper.getList(classOf[Category])
        } then { result =>
          adapterCategory.setData(result) // set categories
          spCategory.setSelection(0, true) // select first
        } otherwise { e =>
          error("failed to load categories", e)
        } execute ()

      // set the selected search
      case id => searchSet(id)
    }
  }

  override def onResume {
    super.onResume

    // clear cached files
    async[Unit, Unit, Unit] { (_, _) => clearFiles } execute ()
  }

  override def onStop {
    val helper = getHelper
    val search = searchUpdate(helper, string(R.string.label_new_search))

    // after update, set this search to recent
    search.recent = 1
    helper.findDao(classOf[Search]).update(search)

    // commit recent id to preferences
    toPref(Query.prefSearch, search.id)

    super.onStop()
  }

  override protected def onCreateDialog(id: Int) = id match {
    // confirm save
    case R.id.confirm =>
      val builder = new AlertDialog.Builder(this)
      val name = dlgName.getText.toString.trim

      // do not allow user to cancel this dialog
      builder.setTitle(R.string.dlg_title_confirm).setCancelable(false)

      val ocl: DialogInterface.OnClickListener = (dialog: DialogInterface, which: Int) => {
        which match {
          // update the search
          case DialogInterface.BUTTON_POSITIVE => executeDb[String, String, Search] { (helper, publish, params) =>
            searchUpdate(helper, params(0))
          } then { search =>
            setSearch(search)
          } otherwise { e =>
            error("could not overwrite the search", e)
          } inform {
            R.string.stat_update
          } execute (name)

          case _ => // do nothing
        }

        removeDialog(R.id.confirm) // remove dialog
      }

      // buttons, message & create
      builder.setPositiveButton(R.string.btn_overwrite, ocl)
      builder.setNegativeButton(R.string.btn_cancel, ocl)
      builder.setMessage(MessageFormat.format(string(R.string.cfm_search), name)).create

    // select neighborhoods
    case R.id.hoods =>
      val builder = new AlertDialog.Builder(this)
      builder.setTitle(R.string.dlg_title_hoods)

      // retrieve labels
      val labels = getHelper.getList(classOf[Hood], "section_id" -> spSection.getSelectedItemId)
        .map(_.name).toArray[CharSequence]

      // init hoods
      hoods match {
        case None => hoods = Option(new Array[Boolean](labels.size))
        case Some(x) => // do nothing
      }

      // set in onPrepare
      builder.setMultiChoiceItems(labels, hoods.get, (dialog: DialogInterface, which: Int, checked: Boolean) => {})

      val ocl: DialogInterface.OnClickListener = (dialog: DialogInterface, which: Int) => which match {
        case DialogInterface.BUTTON_POSITIVE =>
          Arrays.fill(hoods.get, false)
          removeDialog(R.id.hoods)
        case _ => removeDialog(R.id.hoods)
      }

      builder.setPositiveButton(R.string.btn_clear, ocl)
      builder.setNegativeButton(R.string.btn_close, ocl)
      builder.setOnCancelListener((dialog: DialogInterface) => removeDialog(R.id.hoods))
      builder.create

    //    // manage searches
    //    case R.id.manage =>
    //      val builder = new AlertDialog.Builder(this)
    //      builder.setTitle(R.string.dlg_title_manage)
    //
    //      builder.setMultiChoiceItems(cursorSearch, "checked", "name", (dialog: DialogInterface, which: Int, checked: Boolean) => {
    //        executeDb[Unit, Unit, Unit] { (helper, publish, params) =>
    //          // mark searches as checked
    //          val dao = helper.findDao(classOf[Search])
    //          val search = dao.queryForId(dialog.asInstanceOf[AlertDialog].getListView.getItemIdAtPosition(which).toInt)
    //          checked match { case true => search.checked = 1; case false => search.checked = 0 }
    //          dao.update(search)
    //        } then { result =>
    //          cursorSearch.requery
    //        } otherwise { e =>
    //          error("cannot set checked status for search", e)
    //        } execute ()
    //      })
    //
    //      val ocl: DialogInterface.OnClickListener = (dialog: DialogInterface, which: Int) => which match {
    //        case DialogInterface.BUTTON_POSITIVE => searchDelete
    //        case _ => // do nothing
    //      }
    //
    //      builder.setPositiveButton(R.string.btn_delete, ocl)
    //      builder.setNegativeButton(R.string.btn_cancel, ocl)
    //      builder.create

    // select region
    case R.id.region =>
      val builder = new AlertDialog.Builder(this)
      builder.setTitle(R.string.dlg_title_region)
      builder.setSingleChoiceItems(cursorRegion, 0, "name", (dialog: DialogInterface, which: Int) => {
        executeDb[Unit, String, List[Area]] { (helper, publish, params) =>
          region = dialog.asInstanceOf[AlertDialog].getListView.getItemIdAtPosition(which).intValue
          helper.getList(classOf[Area], "region_id" -> region)
        } then { result =>
          adapterArea.setData(result)
          if (!result.isEmpty) spArea.setSelection(0, true)
        } otherwise { e =>
          error("could not load areas", e)
        } execute ()

        dialog.dismiss
      })

      val dialog = builder.create
      dlgRegions = dialog.getListView // capture regions list
      dialog

    //    //  TODO save search
    //    case R.id.save =>
    //      val builder = new AlertDialog.Builder(this)
    //      builder.setTitle(R.string.dlg_title_save)
    //
    //      // create view
    //      val save = getLayoutInflater.inflate(R.layout.dlg_save, null)
    //      builder.setView(save)
    //
    //      // name textview
    //      dlgName = view(R.id.name, save)
    //
    //      // positive button set in onPrepare
    //      val ocl: DialogInterface.OnClickListener = (dialog: DialogInterface, which: Int) => {}
    //
    //      builder.setPositiveButton(R.string.btn_save, ocl)
    //      builder.setNegativeButton(R.string.btn_cancel, ocl)
    //      builder.create

    //    // TODO select search
    //    case R.id.searches =>
    //      val builder = new AlertDialog.Builder(this)
    //      builder.setTitle(R.string.dlg_title_searches)
    //
    //      // set the selection
    //      builder.setSingleChoiceItems(cursorSearch, -1, "name", (dialog: DialogInterface, which: Int) => {
    //        // set selected search
    //        searchSet(dialog.asInstanceOf[AlertDialog].getListView.getItemIdAtPosition(which).intValue)
    //
    //        // dismiss dialog
    //        dialog.dismiss()
    //      })
    //
    //      // capture searches list
    //      val dialog = builder.create
    //      dlgSearches = dialog.getListView
    //      dialog

    // TODO add this back
    //    // warning for database error? R.id.warning
    //    case 500 =>
    //      val builder = new AlertDialog.Builder(this)
    //      builder.setCancelable(false)
    //      builder.setTitle(R.string.dlg_title_db)
    //      builder.setIcon(android.R.drawable.ic_dialog_alert)
    //
    //      val ocl: DialogInterface.OnClickListener = (dialog: DialogInterface, which: Int) => which match {
    //        case DialogInterface.BUTTON_POSITIVE =>
    //          // rate
    //          val intent = new Intent(Intent.ACTION_VIEW)
    //          intent.setData(Uri.parse(string(R.string.shr_rate)))
    //          startActivity(intent)
    //        case _ => // do nothing
    //      }
    //
    //      builder.setPositiveButton(R.string.btn_update, ocl)
    //      builder.setNegativeButton(R.string.btn_cancel, ocl)
    //      builder.setMessage(R.string.warn_db).create()

    // default
    case _ => super.onCreateDialog(id)
  }

  // if save dialog is prepared
  var saved = false

  override def onPrepareDialog(id: Int, dialog: Dialog) {
    // find item in adapter with the given id
    def getItemPosition(adapter: AdapterView[_], id: Int) =
      Range(0, adapter.getCount).find(id == adapter.getItemIdAtPosition(_)).toRight(-1).fold(identity, identity)

    id match {
      // TODO update search cursor
      //case R.id.manage => cursorSearch.requery

      // set selected region
      case R.id.region => dlgRegions.setItemChecked(getItemPosition(dlgRegions, region), true)

      //      // TODO save
      //      case R.id.save =>
      //        // set the dialog name
      //        search match {
      //          // no search selected, set dialog to blank
      //          case Some(x) if x.recent == 1 => dlgName.setText("")
      //          case None => dlgName.setText("")
      //
      //          // set name to title if search selected
      //          case _ =>
      //            val title = getTitle.toString
      //            dlgName.setText(title)
      //            dlgName.setSelection(title.length)
      //            dlgName.selectAll
      //        }
      //
      //        // if saved dialog already prepared, then return
      //        saved match {
      //          case true => // do nothing
      //          case false =>
      //            // add a click listener to positive button to allow for validation
      //            dialog.asInstanceOf[AlertDialog].getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener((v: View) => {
      //              dlgName.getText.toString.trim match {
      //                // no name provided
      //                case "" => toastShort("name is empty")
      //
      //                // name present, continue
      //                case name =>
      //                  // dismiss dialog
      //                  dialog.dismiss
      //
      //                  // find search with name
      //                  executeDb[Unit, Unit, Boolean] { (helper, publish, params) =>
      //                    val dao = helper.findDao(classOf[Search])
      //                    Option(dao.queryForFirst(dao.queryBuilder.where.eq("name", params(0)).prepare)) match {
      //                      case Some(x) => true
      //                      case None => false
      //                    }
      //                  } then {
      //                    // show confirmation to overwrite
      //                    case true => showDialog(R.id.confirm)
      //
      //                    // create/update the search
      //                    case false => executeDb[String, String, Search] { (helper, publish, params) =>
      //                      searchUpdate(helper, params(0))
      //                    } then { search =>
      //                      setSearch(search)
      //                    } otherwise { e =>
      //                      error("could not save the search", e)
      //                    } inform {
      //                      R.string.stat_save
      //                    } execute (name)
      //                  } otherwise { e =>
      //                    error("could not load searches", e)
      //                  } execute (name)
      //              }
      //            })
      //
      //            saved = true
      //        }

      //      // TODO select search
      //      case R.id.searches =>
      //        // update the search cursor
      //        cursorSearch.requery
      //
      //        search match {
      //          // set selected search
      //          case Some(x) => dlgSearches.setItemChecked(getItemPosition(dlgSearches, x.id), true)
      //
      //          // set all items unchecked
      //          case None => Range(0, dlgSearches.getCount).foreach(dlgSearches.setItemChecked(_, false))
      //        }

      // no matches
      case _ => super.onPrepareDialog(id, dialog)
    }
  }

  override def handleMessage(msg: Message): Boolean = msg.what match {
    // show image
    case Imageable.msgShow =>
      val (uri, bitmap) = msg.obj.asInstanceOf[Uri Pair Bitmap]
      val image: ImageView = msg.arg1
      image.setImageBitmap(bitmap)
      image.setVisibility(View.VISIBLE)
      true

    // hide image
    case Imageable.msgHide =>
      (msg.arg1: ImageView).setVisibility(View.GONE)
      true
  }

  override def init {
    // init query
    //query.setText(null)

    // init adapters
    List[BaseSpinnerAdapter[_]](adapterArea, adapterLocale, adapterSection, adapterCategory, adapterSubCategory).foreach(
      _.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item))

    // bind adapters
    spArea.setAdapter(adapterArea)
    spLocale.setAdapter(adapterLocale)
    spSection.setAdapter(adapterSection)
    spCategory.setAdapter(adapterCategory)
    spSubcat.setAdapter(adapterSubCategory)

    // subcategory behavior
    spSubcat.setOnItemSelectedListener(new OnSpinnerItemSelectedListener[SubCategory] {
      override def onSpinnerItemSelected(item: SubCategory, position: Int) = setHeaderCategory
    })

    // category behavior
    spCategory.setOnItemSelectedListener(new OnSpinnerItemSelectedListener[Category] {
      override def onSpinnerItemSelected(item: Category, position: Int) {
        // set criteria according to selected category
        setCriteria

        // manage subcategories
        fetchSubCategories pre { () =>
          enableControls(false)
        } post { () =>
          enableControls()
        } then { result =>
          adapterSubCategory.setData(result)
          updateFilter match {
            case true => spSubcat.setSelectedItemPositionWithValue(subcat)
            case false => spSubcat.setSelection(0, true)
          }
        } otherwise { e =>
          error("could not load subcategories for category", e)
          // just set an all item if failed
          adapterSubCategory.setData(List(new SubCategory("all", item.code)))
        } execute ()
      }
    })

    // section behavior
    spSection.setOnItemSelectedListener(new OnSpinnerItemSelectedListener[Section] {
      override def onSpinnerItemSelected(item: Section, position: Int) {
        updateLocation match {
          // do not update location
          case true => updateLocation = false
          // set selected hoods to none
          case false => hoods = None
        }

        // only enable if hoods position is not first
        btnHoods.setEnabled(item.hoods)

        // set the header
        setHeaderSite(1)
      }
    })

    // locale behavior
    spLocale.setOnItemSelectedListener(new OnSpinnerItemSelectedListener[Locale] {
      override def onSpinnerItemSelected(item: Locale, position: Int) = executeDb[Unit, Unit, List[Section]] { (helper, publish, params) =>
        // load sections
        helper.getList(classOf[Section], "locale_id" -> item.id)
      } post { () =>
        // get subcategories once sections has been retrieved
        fetchSubCategories pre { () =>
          enableControls(false)
        } post { () =>
          enableControls()
        } then { result =>
          // set these subcategories
          adapterSubCategory.setData(result)
          if (updateFilter) {
            // set subcats and initFilter to false
            spSubcat.setSelectedItemPositionWithValue(subcat)
            updateFilter = false
          }

          setHeaderCategory
        } otherwise { e =>
          error("could not load subcategories for locale", e)
          updateFilter = false

          // just set an all item if failed
          // TODO lots of ppl fail right here
          try {
            adapterSubCategory.setData(List(new SubCategory("all", spCategory.getAs(classOf[Category]).code)))
          } catch {
            case e => // showDialog(R.id.warning)
            // toast("please clear app data OR reinstall the application")
          }
        } execute ()
      } then {
        // no sections
        case Nil =>
          adapterSection.reset

          if (loutSection.isShown) loutSection.startAnimation(animOut)

          // disable hoods button
          btnHoods.setEnabled(false)

          // disable section
          spSection.setEnabled(false)

          // update location true
          updateLocation = true

          // set site header
          setHeaderSite(0)

        // sections present
        case result =>
          adapterSection.setData(result)

          if (!loutSection.isShown) {
            loutSection.setVisibility(View.VISIBLE)
            loutSection.startAnimation(animIn)
          }

          // check if we need to initialize
          (updateLocation && section != 0) match {
            // set to the update selected section
            case true => spSection.setSelectedItemPositionWithId(section)
            case false =>
              // set first selection
              spSection.setSelection(0, false)
              // reset hoods
              hoods = None
          }

          // enable section
          spSection.setEnabled(true)
      } otherwise { e =>
        error("could not load sections", e)
      } execute ()
    })

    // area behavior
    spArea.setOnItemSelectedListener(new OnSpinnerItemSelectedListener[Area] {
      override def onSpinnerItemSelected(item: Area, position: Int) = executeDb[Unit, Unit, List[Locale]] { (helper, publish, params) =>
        helper.getList(classOf[Locale], "area_id" -> item.id)
      } then { result =>
        adapterLocale.setData(result)
        updateLocation match {
          // set to the updated locale
          case true => spLocale.setSelectedItemPositionWithId(locale)
          // set to first selection
          case false => spLocale.setSelection(0, false)
        }
      } otherwise { e =>
        error("could not load locales for area")
      } execute ()
    })

    // animations
    animOut.setAnimationListener(new AnimationListener {
      override def onAnimationStart(anim: Animation) {}
      override def onAnimationRepeat(anim: Animation) {}

      // hide section layout
      override def onAnimationEnd(anim: Animation) = loutSection.setVisibility(View.GONE)
    })
  }

  def onControl(v: View) = {
    def hasSearches(f: => Unit) = try {
      val helper = getHelper
      helper.isOpen match {
        case true => helper.findDao(classOf[Search]).queryForEq("recent", 0).isEmpty match {
          case true => toastShort("no searches stored")
          case false => f
        }
        case false => toastShort("database is not open")
      }
    } catch {
      case e =>
        toastShort("cannot access searches")
        error("cannot accesss searches", e)
    }

    v.getId match {
      case R.id.browse => doBrowse
      case R.id.clear => query.setText("")
      case R.id.hoods => showDialog(R.id.hoods)
      //case R.id.manage => hasSearches(showDialog(R.id.manage)) TODO
      case R.id.post => doPost
      case R.id.region => showDialog(R.id.region)
      //case R.id.save => showDialog(R.id.save)
      case R.id.search => doSearch
      //case R.id.searches => hasSearches(showDialog(R.id.searches)) TODO
      case _ => // do nothing
    }
  }

  //  override def onCreateOptionsMenu(menu: Menu) = {
  //    getMenuInflater.inflate(R.menu.activity_main, menu)
  //    true
  //  }

  private def doBrowse {
    // build url
    val url = new StringBuilder(spLocale.getSelectedItemValue)
    url.append(spSection.getSelectedItemValue)
      .append(spSubcat.getSelectedItemValue)
      .append('/')

    // TODO show results
    //val intent = new Intent(Query.this, classOf[Results])
    //intent.putExtra(Results.intentUrl, url.toString)
    //startActivity(intent)
  }

  private def doPost = Option(spLocale.getAs(classOf[Locale])) match {
    case None => // no locale selected, do nothing
    case Some(locale) => Option(locale.code) match {
      // launch posting
      case Some(code) =>
      //			val intent = new Intent(Query.this, classOf[Select])
      //			intent.putExtra(Select.CODE, loc.code)// "https://post.craigslist.org/c/sat?lang=en");
      //			intent.putExtra(Select.TITLE, loc.name)
      //			intent.putExtra(Select.REGION, loc.area.region.name)
      //			startActivity(intent)

      // fetch code
      case None => executeDb[Unit, Unit, String] { (helper, publish, params) =>

        parseNode(locale.url) { feed =>
          locale.code = feed \\ "ul" match {
            // retrieve the 3-letter code from the first link (post) on front page
            case list if (list \ "@id" == "postlks") =>
              (list \ "li" \ "a" apply 0 attribute "href" toString) substring (30, 33)
            case _ => null
          }
        }

        // save & return
        helper.findDao(classOf[Locale]).update(locale)
        locale.code
      } then { result =>
        toastShort("the post code is %s" format result)
        //					// use this post code for post activity
        //					Intent intent = new Intent(Query.this, classOf[Select])
        //					intent.putExtra(Select.CODE, result)// "https://post.craigslist.org/c/sat?lang=en");
        //					intent.putExtra(Select.TITLE, loc.name)
        //					intent.putExtra(Select.REGION, loc.area.region.name)
        //					startActivity(intent)
      } otherwise { e =>
        toastShort("cannot determine post code for this locale")
        error("could not determine post code", e)
      } execute ()
    }
  }

  private def doSearch {
    // get parameters recursively
    def getParameters(view: View): List[String] = view match {
      case group: ViewGroup => group match {
        case spinner: Spinner => List(spinner.getTag.toString + spinner.getSelectedItemPosition)
        case text: ClearableEditText => List(text.getTag.toString + URLEncoder.encode(text.getText.toString.trim, "UTF-8"))
        case _ => Range(0, group.getChildCount).flatMap(pos => getParameters(group.getChildAt(pos))).toList
      }
      case button: CompoundButton if button.isChecked => List(button.getTag.toString)
      case text: EditText => List(text.getTag.toString + URLEncoder.encode(text.getText.toString.trim, "UTF-8"))
      case _ => Nil
    }

    executeDb[Unit, Unit, Option[String]] { (helper, publish, params) =>
      // build query
      val sb = new StringBuilder(spLocale.getSelectedItemValue)
      sb.append("/search").append(spSubcat.getSelectedItemValue)
      sb.append(spSection.getSelectedItemValue).append('?')

      // hoods
      hoods match {
        case Some(x) => helper.findDao(classOf[Hood]).queryForEq("section_id", spSection.getSelectedItemId.toInt).toList match {
          case hds @ h :: hs => for ((checked, hood) <- x.zip(hds)) checked match {
            // include hood in query
            case true => sb.append("nh=").append(hood.id).append('&')
            case false => // hood not checked
          }

          case Nil => // no hoods for current section
        }

        case None => // no selected hoods
      }

      // add parameters
      sb.append(getParameters(loutParameters).reverse.mkString("&"))
      val rv = sb.toString

      // check if this should just be a browse
      // remove blank query string/search type and name of name-value pairs
      val browse = rv.replaceAll("query=&srchType=[A|T]", "").replaceAll("&\\w+=", "")

      browse.substring(browse.indexOf('?')).length match {
        case 1 => None // just a browse
        case _ => Some(rv) // perform search with given query
      }
    } then {
      case Some(x) =>
        warn("the query is %s" format x)
      // TODO launch result activity
      //val intent = new Intent(Query.this, /* Listings.class */Results.class)
      //intent.putExtra(Results.URL, result).putExtra(Results.SORT, SortType.DATE);// .putExtra(Listings.GROUP,
      // 1)
      //intent.putExtra(Results.WORDS, query.getText().toString())
      //startActivity(intent)
      case None => doBrowse // just browse
    } otherwise { e =>
      error("could not initiate search", e)
    } execute ()
  }

  private def enableControls(enabled: Boolean = true) {
    // subcategory
    spSubcat.setEnabled(enabled)

    // browse & search
    btnBrowse.setEnabled(enabled)
    btnSearch.setEnabled(enabled)
    btnPost.setEnabled(enabled)
  }

  // fetch and store subcategories
  private def fetchSubCategories = executeDb[Unit, Unit, List[SubCategory]] { (helper, publish, params) =>
    val category = spCategory.getAs(classOf[Category])

    Option(spLocale.getAs(classOf[Locale])) match {
      // locale selected
      case Some(x) =>
        helper.getList(classOf[SubCategory], "locale_id" -> x.id, "category_id" -> category.id) match {
          case Nil =>
            // fetch subcategories
            val result = new SubCategory("all", category.code) +=:
              parseDoc(x.url concat "/" concat category.code) { doc =>
                def isNotBlank(n: Node) = !TextUtils.isEmpty(n attr "value")
                // create subs
                doc.select("select[name=catAbb] > option")
                  .dropWhile(isNotBlank) // remove until blank
                  .tail // remove blank head
                  .takeWhile(isNotBlank) // take until blank
                  .map(n => new SubCategory(n text, n attr "value"))
              } toList

            // save off subcategories
            executeDb[SubCategory, Unit, Unit] { (helper, publish, params) =>
              val dao = helper.findDao(classOf[SubCategory])
              dao.callBatchTasks(new Callable[Unit] {
                override def call = params foreach { sc =>
                  sc.category = category
                  sc.locale = locale
                  dao.create(sc)
                }
              })
            } execute (result: _*)
            result
          case result => result
        }

      // no locale selected, just return an all item
      case None =>
        warn("no locale selected")
        List(new SubCategory("all", category.code))
    }
  }

  // set parameters (hack since additional params won't display normally)
  private def setCriteria {
    // remove parameters view
    //loutAdditional.setVisibility(View.GONE)
    loutAdditional.removeAllViews

    // get category layout
    val lout = try louts(spCategory.getSelectedItemPosition) catch { case _ => 0 }
    //val filter = try layouts(spCategory.getSelectedItemPosition) catch { case _ => None }
    //louts(spCategory.getSelectedItemPosition) catch { case _ => 0 }

    //    filter match {
    //      case Some(x) if x.getId == R.id.housing_search =>
    //        val params = new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER)
    //        //loutAdditional.addView(x, params)
    //        loutAdditional.addView(x, params)
    //        x.getParent.bringChildToFront(x)
    //        x.setVisibility(View.VISIBLE)
    //        //loutAdditional.setVisibility(View.VISIBLE)
    //
    //        val bedroom: Spinner = view(R.id.bedrooms, x)
    //        val adapter = new ArrayAdapter[String](this, android.R.layout.simple_spinner_item, getResources.getStringArray(R.array.bedrooms))
    //        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    //        bedroom.setAdapter(adapter)
    //
    //      case Some(x) =>
    //        val params = new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER)
    //        loutAdditional.addView(x, params)
    //        x.getParent.bringChildToFront(x)
    //        x.setVisibility(View.VISIBLE)
    //        //loutAdditional.setVisibility(View.VISIBLE)
    //
    //      case None => // do nothing
    //    }

    if (lout != 0) {
      // all searches
      //val filter = layouts(2).get
      //inflate(View view, int resource, ViewGroup root)
      //val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE).asInstanceOf[LayoutInflater]
      val filter = getLayoutInflater.inflate(lout, null)

      async[Filter, Unit, Unit] { (_, _) => } then { _ =>
        val params = new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER)
        filter.setLayoutParams(params)
        loutAdditional.addView(filter, params)
      } execute ()
      //loutAdditional.setLayoutParams(params)
      //filter.setLayoutParams(params)

      //val laparams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
      //loutAdditional.setLayoutParams(laparams)
      //loutAdditional.setVisibility(View.VISIBLE)

      // housing
      if (lout == R.layout.search_housing) {
        val bedroom: Spinner = view(R.id.bedrooms, filter)
        val adapter = new ArrayAdapter[String](this, org.holoeverywhere.R.layout.simple_spinner_item, getResources.getStringArray(R.array.bedrooms))
        adapter.setDropDownViewResource(org.holoeverywhere.R.layout.simple_spinner_dropdown_item)
        bedroom.setAdapter(adapter)
      }
    }

    search match {
      case Some(s) if updateFilter =>
        // standard
        query.setText(s.filter.query)
        query.setSelection(s.filter.query.length)

        // post/title
        s.filter.inTitle match {
          case true => (R.id.title: RadioButton).setChecked(true)
          case false => (R.id.post: RadioButton).setChecked(true)
        }

        // image
        (R.id.image: CheckBox).setChecked(s.filter.hasImage)

        // specific
        lout match {
          case R.layout.search_gigs =>
            (view(List(R.id.all, R.id.pay, R.id.nopay)(s.filter.pay.intValue), loutAdditional): RadioButton).setChecked(true)
          case R.layout.search_jobs =>
            // TODO why aren't we looking for the buttons in loutAdditional? this is probably a BUG!
            val jobs = List(R.id.tele, R.id.cont, R.id.intern, R.id.pt, R.id.np)
            jobs foreach { job =>
              //(view(job, loutAdditional): CheckBox).setChecked((s.filter.jobs.intValue & (1 << jobs.indexOf(job))) > 0)
              (job: CheckBox).setChecked((s.filter.jobs.intValue & (1 << jobs.indexOf(job))) > 0)
            }
          case R.layout.search_housing =>
            (view(R.id.min, loutAdditional): EditText).setText(s.filter.min)
            (view(R.id.max, loutAdditional): EditText).setText(s.filter.max)
            (view(R.id.cats, loutAdditional): CheckBox).setChecked(s.filter.cats)
            (view(R.id.dogs, loutAdditional): CheckBox).setChecked(s.filter.dogs)
            (view(R.id.bedrooms, loutAdditional): Spinner).setSelection(s.filter.beds.intValue, true)
          case R.layout.search_personals =>
            (view(R.id.min, loutAdditional): EditText).setText(s.filter.min)
            (view(R.id.max, loutAdditional): EditText).setText(s.filter.max)
          case R.layout.search_sale =>
            (view(R.id.min, loutAdditional): EditText).setText(s.filter.min)
            (view(R.id.max, loutAdditional): EditText).setText(s.filter.max)
          case _ => // do nothing
        }
      case _ => // do nothing
    }
  }

  // set category header
  private def setHeaderCategory = hdrCategory setText {
    "%s : %s" format
      (spCategory.getAs(classOf[Category]).name, spSubcat.getAs(classOf[SubCategory]).name)
  }

  // set site header
  private def setHeaderSite(depth: Int) = depth match {
    case 0 => hdrSite setText spLocale.getAs(classOf[Locale]).name
    case 1 => hdrSite setText {
      "%s : %s" format (spLocale.getAs(classOf[Locale]).name, spSection.getAs(classOf[Section]).name)
    }
    case _ => // do nothing
  }

  private def setSearch(search: Search) {
    this.search = Option(search)
    search.recent match {
      case 0 => setTitle(search.name)
      case _ => // do nothing
    }
  }

  // delete the search
  private def searchDelete = executeDb[Unit, Unit, Boolean] { (helper, publish, params) =>
    // retrieve all checked searches
    val daoSearch = helper.findDao(classOf[Search])
    val searches = daoSearch.queryForEq("checked", 1)

    // delete checked hoods
    val daoChecked = helper.findDao(classOf[CheckedHood])
    val db = daoChecked.deleteBuilder
    db.where.in("location_id", searches.map(_.location.id))
    daoChecked.delete(db.prepare)

    helper.findDao(classOf[Location]).delete(searches.map(_.location)) // delete locations
    helper.findDao(classOf[Filter]).delete(searches.map(_.filter)) // delete filters
    daoSearch.delete(searches) // delete searches

    // has the current search been deleted?
    search match {
      case Some(x) if daoSearch.queryForId(x.id) == null => true
      case _ => false
    }
  } then {
    // active search deleted
    case true =>
      search = None
      setTitle(R.string.label_new_search)
    case false => // do nothing
  } otherwise { e =>
    error("could not delete searches", e)
  } execute ()

  // set the search with the given id
  private def searchSet(id: Int) = executeDb[Int, Unit, Search] { (helper, publish, params) =>
    val result = helper.findDao(classOf[Search]).queryForId(params(0))

    Option(result.location.section) match {
      // configure with section  
      case Some(x) =>
        val dao = helper.findDao(classOf[Section])
        Option(dao.queryForFirst(dao.queryBuilder.where.eq("code", x).prepare)) match {
          // reset section
          case None => section = 0
          case Some(sect) =>
            section = sect.id // configure section
            locale = sect.locale.id // configure locale
            area = sect.locale.area.id // configure area
            targetRegion = sect.locale.area.region.id // configure region

            if (sect.hoods) { // capture the checked hoods
              val checked = helper.findDao(classOf[CheckedHood]).queryForEq("location_id", result.location.id)
              hoods = Some(helper.findDao(classOf[Hood]).queryForEq("section_id", section).map { hood =>
                checked.exists(_.hood == hood.id)
              }.toArray[Boolean])
            }
        }

      // configure locale only
      case None =>
        val dao = helper.findDao(classOf[Locale])
        Option(dao.queryForFirst(dao.queryBuilder.where.eq("url", result.location.url).prepare)) match {
          // configure locale
          case Some(x) =>
            locale = x.id // configure locale
            area = x.area.id // configure area
            targetRegion = x.area.region.id // configure region
          case None => // do nothing
        }

        // reset section
        section = 0
    }

    // configure category & subcategory
    val dao = helper.findDao(classOf[Category])
    category = dao.queryForFirst(dao.queryBuilder.where.eq("code", result.filter.code)
      .prepare).id
    subcat = "/%s" format result.filter.subcatCode

    result
  } pre { () =>
    // init selections
    area = 0
    category = 0
    targetRegion = -1
  } then { result =>
    // set the search
    setSearch(result)

    // set initialized to false (to update spinners)
    updateFilter = true
    updateLocation = true

    // only update if the region has changed
    if (targetRegion == region) spArea.setSelectedItemPositionWithId(area)
    // update the area, different region
    else executeDb[Int, Unit, List[Area]] { (helper, publish, params) =>
      region = params(0)
      helper.getList(classOf[Area], "region_id" -> region)
    } then { result =>
      adapterArea.setData(result)
      spArea.setSelectedItemPositionWithId(area)
    } otherwise { e =>
      error("failed to load areas for selected region", e)
    } execute (targetRegion)

    Option(spCategory.getSelectedItem) match {
      // select the category
      case Some(x) => spCategory.setSelectedItemPositionWithId(category)

      // update the categories
      case None => executeDb[Unit, Unit, List[Category]] { (helper, publish, params) =>
        helper.findDao(classOf[Category]).queryForAll.toList
      } then { result =>
        adapterCategory.setData(result)
        spCategory.setSelectedItemPositionWithId(category)
      } otherwise { e =>
        error("could not load categories", e)
      } execute ()
    }
  } otherwise { e =>
    error("could not load the search to set", e)
  } execute (id)

  // update the search with the given id (f is function to perform in background)
  private def searchUpdate(helper: DatabaseHelper, name: String) = {
    val daoSearch = helper.findDao(classOf[Search])
    var exists = true
    val search = Option(daoSearch.queryForFirst(daoSearch.queryBuilder.where.eq("name", name).prepare)) match {
      case Some(x) => x
      case None => { exists = false; new Search(name) }
    }

    // location
    val location = exists match {
      case true => search.location
      case false => new Location
    }

    // set url
    location.url = spLocale.getSelectedItemValue

    val daoLoc = helper.findDao(classOf[Location])
    val daoChecked = helper.findDao(classOf[CheckedHood])

    exists match {
      // manage checked hoods
      case true =>
        location.section = null
        val delete = daoChecked.deleteBuilder
        delete.where.eq("location_id", location.id)
        daoChecked.delete(delete.prepare)

      // create location
      case false =>
        daoLoc.create(location)
        search.location = location
    }

    // prepare section if present and not first item
    if (spSection.getSelectedItemPosition > 0) {
      // section is selected, set the value
      val id = spSection.getSelectedItemId.toInt
      val section = helper.findDao(classOf[Section]).queryForId(id)
      location.section = section.code

      // create any selected hoods
      hoods match {
        case Some(x) if section.hoods =>
          // create tuples of selected w/ hoods
          val results = x.zip(helper.findDao(classOf[Hood]).queryForEq("section_id", id))
          daoChecked.callBatchTasks(new Callable[Unit] {
            override def call = for ((checked, hood) <- results)
              checked match {
                case true => daoChecked.create(new CheckedHood(location, hood.id))
                case false => // do nothing
              }
          })
        case _ => // do nothing
      }
    }

    // update location
    daoLoc.update(location)

    // filter
    val filter = exists match {
      case true => search.filter.clear
      case false => new Filter
    }

    // category & subcategory
    filter.code = spCategory.getAs(classOf[Category]).code
    filter.subcatCode = spSubcat.getAs(classOf[SubCategory]).code

    // standard fields
    filter.query = query.getText.toString.trim
    filter.inTitle = (R.id.title: RadioButton).isChecked
    filter.hasImage = (R.id.image: CheckBox).isChecked

    louts(spCategory.getSelectedItemPosition) match {
      // set gigs
      case R.layout.search_gigs => (view(R.id.gigs, loutAdditional): RadioGroup).getCheckedRadioButtonId match {
        case R.id.all => filter.pay = new Integer(0)
        case R.id.pay => filter.pay = new Integer(1)
        case R.id.nopay => filter.pay = new Integer(2)
      }

      // jobs as a bitmask of options
      case R.layout.search_jobs => filter.jobs = List(R.id.tele, R.id.cont, R.id.intern, R.id.pt, R.id.np)
        .map(id => (id: CheckBox).isChecked match { case true => 1; case false => 0 })
        .reduceLeft((a, b) => (a << 1) + b).asInstanceOf[Integer]

      // housing
      case R.layout.search_housing =>
        filter.cats = (view(R.id.cats, loutAdditional): CheckBox).isChecked
        filter.dogs = (view(R.id.dogs, loutAdditional): CheckBox).isChecked
        filter.beds = (view(R.id.bedrooms, loutAdditional): Spinner).getSelectedItemPosition.asInstanceOf[Integer]

      // personals
      case R.layout.search_personals =>
        filter.min = (view(R.id.min, loutAdditional): EditText).getText.toString
        filter.max = (view(R.id.max, loutAdditional): EditText).getText.toString

      // sale
      case R.layout.search_sale =>
        filter.min = (view(R.id.min, loutAdditional): EditText).getText.toString
        filter.max = (view(R.id.max, loutAdditional): EditText).getText.toString

      // do nothing
      case _ =>
    }

    // update filter/search
    exists match {
      // update
      case true =>
        helper.findDao(classOf[Filter]).update(filter)
        daoSearch.update(search)

      // create
      case false =>
        helper.findDao(classOf[Filter]).create(filter)
        search.filter = filter
        daoSearch.create(search)
    }

    search
  }

  // headers
  lazy val hdrCategory: TextView = R.id.hdr_category
  lazy val hdrSite: TextView = R.id.hdr_site

  // layouts
  lazy val loutAdditional: FrameLayout = R.id.additional
  lazy val loutParameters: RelativeLayout = R.id.parameters
  lazy val loutSection: LinearLayout = R.id.section_lo

  // text
  lazy val query: ClearableEditText = R.id.query

  // buttons
  lazy val btnBrowse: Button = R.id.browse
  lazy val btnSearch: Button = R.id.search
  lazy val btnPost: Button = R.id.posting
  lazy val btnHoods: ImageButton = R.id.hoods

  // spinners
  lazy val spArea: EventSpinner = R.id.area
  lazy val spLocale: EventSpinner = R.id.locale
  lazy val spSection: EventSpinner = R.id.section
  lazy val spCategory: EventSpinner = R.id.category
  lazy val spSubcat: EventSpinner = R.id.subcat

  // animations
  lazy val animIn = anim(org.holoeverywhere.R.anim.grow_fade_in_from_bottom) //android.R.anim.slide_in_left)
  lazy val animOut = anim(org.holoeverywhere.R.anim.shrink_fade_out_from_bottom) //android.R.anim.slide_out_right)

  // cursors
  lazy val cursorRegion = getHelper.findCursor(classOf[Region], None)
  lazy val cursorSearch = getHelper.findCursor(classOf[Search], None, "recent" -> 0)

  // adapters
  lazy val adapterArea = new BaseSpinnerAdapter[Area](this, org.holoeverywhere.R.layout.simple_spinner_item)
  lazy val adapterLocale = new BaseSpinnerAdapter[Locale](this, org.holoeverywhere.R.layout.simple_spinner_item)
  lazy val adapterSection = new BaseSpinnerAdapter[Section](this, org.holoeverywhere.R.layout.simple_spinner_item)
  lazy val adapterCategory = new BaseSpinnerAdapter[Category](this, org.holoeverywhere.R.layout.simple_spinner_item)
  lazy val adapterSubCategory = new BaseSpinnerAdapter[SubCategory](this, org.holoeverywhere.R.layout.simple_spinner_item)

  // search layouts
  lazy val louts = List(
    0, // community
    0, // events
    R.layout.search_gigs,
    R.layout.search_housing,
    R.layout.search_jobs,
    R.layout.search_personals,
    0, // resumes
    R.layout.search_sale,
    0 // services
    )

  lazy val layouts = List(
    None, // community
    None, // events
    Option(getLayoutInflater.inflate(R.layout.search_gigs, loutAdditional)),
    Option(getLayoutInflater.inflate(R.layout.search_housing, loutAdditional)),
    Option(getLayoutInflater.inflate(R.layout.search_jobs, loutAdditional)),
    Option(getLayoutInflater.inflate(R.layout.search_personals, loutAdditional)),
    None, // resumes
    Option(getLayoutInflater.inflate(R.layout.search_sale, loutAdditional)),
    None) // services

  // mutable

  // dialogs
  var dlgRegions: ListView = _
  var dlgSearches: ListView = _
  var dlgName: EditText = _

  // state
  var search: Option[Search] = None
  var updateFilter = false
  var updateLocation = false
  var hoods: Option[Array[Boolean]] = None
  var region = -1
  var locale: Int = _
  var section: Int = _
  var subcat: String = _

  // just used in search updates
  var area = 0
  var category = 0
  var targetRegion = -1
}

object Query {
  val prefRate = "sce.zl.query.pref.Rate"
  val prefSearch = "sce.zl.query.pref.Search"
}