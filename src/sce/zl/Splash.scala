package sce.zl
import java.util.concurrent._

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import sce.zl.db._
import sce.zl.traits._

class Splash extends Activity
  with Blockable
  with Databasable
  with Intentable
  with Parsable
  with Viewable {

  def string(id: Int) = getString(id)

  lazy val status: TextView = R.id.splash_status

  // state
  var initialized: Boolean = false
  var latch: CountDownLatch = _

  // counts
  val locationCount = 710 // number of locations to expect
  val totalCount = 1134 // number of total locations, hoods, categories, etc. to expect

  override def onCreate(state: Bundle) {
    super.onCreate(state)

    // set layout
    setContentView(R.layout.act_splash)

    ifNotBlocked {
      // whether to force initialize
      fromIntent(Splash.intentInit)(initialized = _: Boolean)
      //val init = getIntent.getBooleanExtra(Splash.intentInit, false)

      // initialization check
      executeDb[String, Unit, Boolean] { (helper, publish, params) =>
        // determine count of Locales
        !initialized && helper.findDao(classOf[Locale]).countOf > locationCount
      } otherwise { e =>
        error("failed finding locale count", e)
        finish
      } then {
        // close and start next activity
        case true =>
          finish
          startActivity(new Intent(Splash.this, classOf[Query]))

        // initialize
        case false => initialize
      } execute ()
    }
  }

  private def initialize = executeDb[String, String, Boolean] { (helper, publish, params) =>
    helper.clean // clean database
    latch = new CountDownLatch(totalCount) // prep latch
    loadCategories // load categories
    loadLocations // load locations

    // 1 minute to finish
    try latch.await(10, TimeUnit.SECONDS)
    catch {
      case e =>
        // exception caught, just return false
        error("initialization could not complete", e)
        false
    }
  } otherwise { e =>
    error("could not perform initialization", e)
    finish
  } then {
    // everything succeeded
    case true =>
      status.setText("succeeded")

      // initialized & no force (i.e., not from settings)
      if (!initialized)
        startActivity(new Intent(Splash.this, classOf[Query]))

      // finish
      finish

    // failed
    case false =>
      status.setText("failed")
      finish
  } execute ()

  //override def init = status.setText(R.string.stat_init)

  // load categories
  private def loadCategories = executeDb[String, String, Unit] { (helper, publish, params) =>
    val daoCat = helper.findDao(classOf[Category])
    val daoSub = helper.findDao(classOf[SubCategory])

    daoCat callBatchTasks new Callable[Unit] {
      override def call = parseNode(params(0)) { feed =>
        for (c <- feed \ "body" \ "div") {
          val category = new Category(c \ "@id" text, c \ "@value" text)
          daoCat.create(category)
          publish(category.name)
        }
      }
    }
  } update { progress =>
    status.setText(progress(0))
    latch.countDown
  } execute (string(R.string.url_categories))

  // load locations
  private def loadLocations = executeDb[String, String, Unit] { (helper, publish, params) =>
    val daoReg = helper.findDao(classOf[Region])
    val daoArea = helper.findDao(classOf[Area])
    val daoLoc = helper.findDao(classOf[Locale])
    val daoSect = helper.findDao(classOf[Section])
    val daoHood = helper.findDao(classOf[Hood])

    daoReg callBatchTasks new Callable[Unit] {
      override def call = parseNode(params(0)) { feed =>
        // regions
        for (r <- feed \ "body" \ "div") {
          val region = new Region(r \ "@id" text);
          daoReg.create(region)
          publish(region.name)

          // areas
          for (a <- r \ "div") {
            val area = new Area(a \ "@id" text)
            area.region = region
            daoArea.create(area)
            publish(area.name)

            // locales
            for (l <- a \ "div") {
              val locale = new Locale(l \ "@id" text, l \ "@value" text)
              locale.area = area
              daoLoc.create(locale)
              publish(locale.name)

              // adding all element
              if (!(l \ "div").isEmpty) {
                val section = new Section("all", "")
                section.locale = locale
                section.hoods = false
                daoSect.create(section)
              }

              // sections
              for (s <- l \ "div") {
                val section = new Section(s \ "@id" text, s \ "@value" text)
                section.locale = locale
                section.hoods = !(s \ "div").isEmpty
                daoSect.create(section)
                publish(section.name)

                // hoods
                for (h <- s \ "div") {
                  val hood = new Hood(h \ "@id" text, (h \ "@value").text.toInt);
                  hood.section = section
                  daoHood.create(hood)
                  publish(hood.name)
                }
              }
            }
          }
        }
      }
    }
  } update { progress =>
    status.setText(progress(0))
    latch.countDown
  } execute (string(R.string.url_locations))
}

object Splash {
  val intentInit = "sc.ezl.splash.intent.Init"
}