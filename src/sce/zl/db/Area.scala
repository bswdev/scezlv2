package sce.zl.db

import java.io.Serializable
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import sce.zl.traits.Identifiable

@DatabaseTable(tableName = "area")
class Area(_name: String) extends Identifiable[Int] with Serializable {
  
	@DatabaseField(generatedId = true, columnName = "_id")
	private var _id: Int = _

	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "region_id", index = true)
	var region: Region = _

	@DatabaseField(canBeNull = false, columnName = "name")
	var name = _name

	// @ForeignCollectionField(eager = false)
	// public ForeignCollection<Locale> locales;

	def this() = this("")
	
	override def id = _id
	
	override def value = name
	
	override def toString = name
}