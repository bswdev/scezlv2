package sce.zl.db

import com.j256.ormlite.field.DatabaseField
import sce.zl.traits.Identifiable
import java.io.Serializable

class Bookmark(url: String) extends Identifiable[String] with Serializable {

  /** url of ad */
  @DatabaseField(id = true, columnName = "_id")
  private var _id = url

  // @DatabaseField(canBeNull = false, columnName = "url")
  // public String url;

  /** checked status, for managing bookmarks */
  @DatabaseField(columnName = "checked")
  var checked = 0

  @DatabaseField(columnName = "time")
  var time = System.currentTimeMillis

  def this() = this("")
  
  override def id = _id

  override def value = _id

  override def toString = _id
}