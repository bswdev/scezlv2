package sce.zl.db
import com.j256.ormlite.field.DatabaseField
import java.io.Serializable
import com.j256.ormlite.table.DatabaseTable
import sce.zl.traits.Identifiable

@DatabaseTable(tableName = "category")
class Category(_name: String, _code: String) extends Identifiable[Int] with Serializable {

  @DatabaseField(generatedId = true, columnName = "_id")
  private var _id: Int = _

  @DatabaseField(canBeNull = false, columnName = "name")
  var name = _name

  /** 3-letter code */
  @DatabaseField(canBeNull = false, columnName = "code")
  var code = _code

  def this() = this("", "")

  override def id = _id

  override def value = "/%s" format code

  override def toString = name
}