package sce.zl.db
import java.io.Serializable
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import sce.zl.traits.Identifiable

@DatabaseTable(tableName = "checked_hood")
class CheckedHood(_location: Location, _hood: Int) extends Identifiable[Int] with Serializable {
  	
  @DatabaseField(generatedId = true, columnName = "_id")
	private var _id: Int = _

	/** search by given location */
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "location_id", index = true)
	var location = _location

	@DatabaseField(columnName = "hood", canBeNull = false)
	var hood = _hood
	
	def this() = this(null, 0)

	override def id = _id
	
	override def value = ""
}