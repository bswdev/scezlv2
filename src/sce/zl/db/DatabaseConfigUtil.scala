package sce.zl.db
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil

object DatabaseConfigUtil extends OrmLiteConfigUtil {
  def main(args: Array[String]) {
    OrmLiteConfigUtil.writeConfigFile("ormlite_config.txt", Array[Class[_]](

      // location
      classOf[Region],
      classOf[Area],
      classOf[Locale],
      classOf[Section],
      classOf[Hood],

      // search
      classOf[Category],
      classOf[SubCategory],
      classOf[Filter],
      classOf[Location],
      classOf[Search],
      classOf[CheckedHood],

      // bookmarks/results
      classOf[Bookmark],
      classOf[HistoryItem]))
  }
}