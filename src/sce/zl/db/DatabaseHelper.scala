package sce.zl.db
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import com.j256.ormlite.support.DatabaseConnection
import com.j256.ormlite.stmt.StatementBuilder.StatementType
import com.j256.ormlite.android.AndroidCompiledStatement
import android.app.Activity
import com.j256.ormlite.stmt.Where
import com.j256.ormlite.dao.CloseableIterator
import scala.collection.JavaConversions._
import sce.zl.traits._
import sce.zl.R

class DatabaseHelper(context: Context)
  //extends OrmLiteSqliteOpenHelper(context, DatabaseHelper.databaseName, null, DatabaseHelper.databaseVersion, R.raw.ormlite_config)
  extends OrmLiteSqliteOpenHelper(context, DatabaseHelper.databaseName, null, DatabaseHelper.databaseVersion, R.raw.ormlite_config)
  with Loggable {

  var connection: Option[DatabaseConnection] = None

  override def onCreate(db: SQLiteDatabase, source: ConnectionSource) = try {
    info("creating database")

    List(
      // location
      classOf[Region],
      classOf[Area],
      classOf[Locale],
      classOf[Section],
      classOf[Hood],

      // search
      classOf[Category],
      classOf[SubCategory],
      classOf[Filter],
      classOf[Location],
      classOf[Search],
      classOf[CheckedHood],

      // bookmarks & results
      classOf[Bookmark],
      classOf[HistoryItem])
      .foreach(TableUtils.createTable(source, _))
  } catch {
    case e => error("cannot create database", e)
  }

  override def onUpgrade(db: SQLiteDatabase, source: ConnectionSource, oldVersion: Int, newVersion: Int) = try {
    info("upgrading database to version %s" format newVersion)

    newVersion match {
      case 2 =>
      // added the history item class
      //TableUtils.createTable(source, HistoryItem.class);
      case _ => // do nothing
    }

    // // rebuild data model
    // TableUtils.dropTable(source, Region.class, true);
    // TableUtils.dropTable(source, Area.class, true);
    // TableUtils.dropTable(source, Locale.class, true);
    // after we drop the old databases, we create the new ones
    // onCreate(db, source);
  } catch {
    case e => error("unable to drop databases for upgrading", e)
    //throw new RuntimeException(e);
  }

  def clean {
    val source = getConnectionSource

    try {
      // rebuild these
      val classes = List(
        // locations
        classOf[Section],
        classOf[Hood],
        classOf[Locale],
        classOf[Area],
        classOf[Region],

        // search
        classOf[SubCategory],
        classOf[Category])

      classes.foreach(TableUtils.clearTable(source, _))

      // IGNORE THESE!
      // TableUtils.clearTable(source, Listing.class);

      // save the searches & bookmarks!
      // TableUtils.clearTable(source, Bookmark.class);
      // TableUtils.clearTable(source, HistoryItem.class);
      // TableUtils.clearTable(source, Search.class);
      // TableUtils.clearTable(source, Filter.class);
      // TableUtils.clearTable(source, Location.class);
      // TableUtils.clearTable(source, LocationHood.class);
      source.close
    } catch {
      case e => error("unable to clear tables for cleaning", e)
    }
  }

  def findDao[T <% Identifiable[ID], ID](entity: Class[T]) = DatabaseHelper.daos.get(entity) match {
    case Some(dao) => dao.asInstanceOf[Dao[T, ID]]
    case None =>
      // create dao & return
      // following line WILL NOT WORK if ascription is not provided
      val dao: Dao[T, ID] = getDao(entity)
      DatabaseHelper.daos += entity -> dao
      dao
  }

  def findCursor[T <% Identifiable[ID], ID](entity: Class[T], asc: Option[Boolean], filters: Pair[String, AnyVal]*): Cursor = {
    // init connection
    connection match {
      case Some(x) => // do nothing
      case None => connection = Option(getConnectionSource.getReadOnlyConnection)
    }

    val builder = findDao(entity).queryBuilder

    // sorting by time
    asc match {
      case Some(x) => builder.orderBy("time", x)
      case None => // do nothing
    }

    // filters
    filters.foreach { filter =>
      val (name, condition) = filter
      builder.where.eq(name, condition)
    }

    // this only works under Android
    val cursor = builder.prepare
      .compile(connection.get, StatementType.SELECT).asInstanceOf[AndroidCompiledStatement]
      .getCursor
    DatabaseHelper.cursors += cursor
    cursor
  }

  def getList[T <% Identifiable[ID], ID](entity: Class[T], queries: (String Pair Any)*) = (queries.length match {
    case 0 => findDao(entity).iterator
    case _ =>
      val dao = findDao(entity)
      val qb = dao.queryBuilder.where.eq(queries.head._1, queries.head._2)
      queries.tail foreach { case (property, value) => qb.and.eq(property, value) }
      dao.iterator(qb.prepare)
  }) toList

  override def close {
    super.close

    // cleanup cursors
    DatabaseHelper.cursors.foreach(_.close)
    DatabaseHelper.cursors.clear

    // clear daos
    DatabaseHelper.daos.clear

    // close connection
    connection match {
      case Some(x) => try {
        x.close
        connection = None
      } catch {
        case e => warn("could not close db connection", e)
      }
      case None => // do nothing
    }
  }
}

object DatabaseHelper {
  val databaseName = "sce.zl"

  // any time you make changes to your database objects, you may have to
  // increase the database version
  val databaseVersion = 1

  lazy val daos: Map[Class[_], Dao[_, _]] = Map[Class[_], Dao[_, _]]()

  lazy val cursors: ListBuffer[Cursor] = ListBuffer[Cursor]()
}