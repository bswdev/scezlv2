package sce.zl.db
import com.j256.ormlite.field.DatabaseField
import sce.zl.traits.Identifiable
import java.io.Serializable
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "filter")
class Filter(_subcatCode: String) extends Identifiable[Int] with Serializable {

  @DatabaseField(generatedId = true, columnName = "_id")
  private var _id: Int = _

  /** 3-letter code to category */
  @DatabaseField(canBeNull = false, columnName = "code")
  var code: String = _

  /** 3-letter code to subcategory */
  @DatabaseField(canBeNull = false, columnName = "subcat_code")
  var subcatCode = _subcatCode

  @DatabaseField(canBeNull = false, columnName = "query")
  var query: String = _

  @DatabaseField
  var hasImage = false;

  @DatabaseField
  var inTitle = false;

  // various (sale, housing, personals)
  @DatabaseField(canBeNull = true, columnName = "min")
  var min: String = null

  @DatabaseField(canBeNull = true, columnName = "max")
  var max: String = null

  /** gigs (0 = all, 1 = pay, 2 = nopay) */
  @DatabaseField(canBeNull = true, columnName = "pay")
  var pay: Integer = null

  // housing
  @DatabaseField(canBeNull = true, columnName = "beds")
  var beds: Integer = null

  @DatabaseField(canBeNull = true, columnName = "cats")
  var cats: Boolean = _ // should be null

  @DatabaseField(canBeNull = true, columnName = "dogs")
  var dogs: Boolean = _ // should b enull

  /** jobs, use a bitmask here (0 = telework, 1 = contract...) */
  @DatabaseField(canBeNull = true, columnName = "jobs")
  var jobs: Integer = null

  def this() = this("")

  /**
   * Reset all the query parameters.
   */
  def clear: Filter = {
    min = null
    max = null
    pay = null
    beds = null
    cats = false //null
    dogs = false //null
    jobs = null
    this
  }

  override def id = _id

  override def value = code
}