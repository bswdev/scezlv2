package sce.zl.db
import com.j256.ormlite.field.DatabaseField
import sce.zl.traits.Identifiable
import java.io.Serializable
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "history")
class HistoryItem(url: String) extends Identifiable[String] with Serializable {

  /** url of ad */
  @DatabaseField(id = true, columnName = "_id")
  private var _id = url

  /** time this was visited */
  @DatabaseField(columnName = "time")
  var time = System.currentTimeMillis

  // no-arg constructor
  def this() = this("")
  
  override def id = _id

  override def value = _id

  override def toString = _id
}