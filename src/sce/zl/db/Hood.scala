package sce.zl.db
import sce.zl.traits.Identifiable
import java.io.Serializable
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "hood")
class Hood(_name: String, __id: Int) extends Identifiable[Int] with Serializable {
  
	/** set the id according to values */
	@DatabaseField(id = true, generatedId = false, columnName = "_id")
	private var _id = __id
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "section_id", index = true)
	var section: Section = _

	@DatabaseField(canBeNull = false, columnName = "name")
	var name = _name

	def this() = this("", 0)

	override def id = _id
	
	override def value = _id.toString
	
	override def toString = name
}