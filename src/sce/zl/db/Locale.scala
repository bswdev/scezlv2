package sce.zl.db

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import sce.zl.traits.Identifiable
import java.io.Serializable

@DatabaseTable(tableName = "locale")
class Locale(_name: String, _url: String) extends Identifiable[Int] with Serializable {

  @DatabaseField(generatedId = true, columnName = "_id")
  private var _id: Int = _

  @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "area_id", index = true)
  var area: Area = _

  @DatabaseField(canBeNull = false, columnName = "name")
  var name = _name

  @DatabaseField(canBeNull = false, columnName = "url", index = true)
  var url = _url

  /** 3-letter code, for posting */
  @DatabaseField(canBeNull = true, columnName = "code")
  var code: String = _

  def this() = this("", "")

  override def id = _id

  override def value = url

  override def toString = name
}