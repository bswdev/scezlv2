package sce.zl.db
import java.io.Serializable
import com.j256.ormlite.field.DatabaseField
import sce.zl.traits.Identifiable
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "location")
class Location extends Identifiable[Int] with Serializable {
  
	@DatabaseField(generatedId = true, columnName = "_id")
	private var _id: Int = _

	/** url to locale */
	@DatabaseField(canBeNull = false, columnName = "url")
	var url: String = _

	/** 3-letter code to section */
	@DatabaseField(canBeNull = true, columnName = "section")
	var section: String = _

	// @ForeignCollectionField(eager = true)
	// public ForeignCollection<CheckedHood> hoods;

	override def id = _id
	
	override def value = url	
}