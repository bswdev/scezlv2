package sce.zl.db

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import sce.zl.traits.Identifiable
import java.io.Serializable

@DatabaseTable(tableName = "region")
class Region(_name: String) extends Identifiable[Int] with Serializable {

  // for QueryBuilder to be able to find the fields
  // public static final String NAME_FIELD_NAME = "name";
  // public static final String PASSWORD_FIELD_NAME = "passwd";

  @DatabaseField(generatedId = true, columnName = "_id")
  private var _id: Int = _

  @DatabaseField(canBeNull = false, columnName = "name")
  var name = _name

  //@ForeignCollectionField(eager = false)
  //public ForeignCollection<State> states;

  def this() = this("")

  override def id = _id
  
  override def value = name
  
  override def toString = name
}