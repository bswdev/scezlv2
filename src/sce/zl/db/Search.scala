package sce.zl.db

import java.io.Serializable
import sce.zl.traits.Identifiable
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "search")
class Search(_name: String, _location: Location, _filter: Filter) extends Identifiable[Int] with Serializable {

	@DatabaseField(generatedId = true, columnName = "_id")
	private var _id: Int = _

	/** search name */
	@DatabaseField(canBeNull = false, columnName = "name", index = true)
	var name = _name

	/** checked status, for managing searches */
	@DatabaseField(columnName = "checked")
	var checked = 0

	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "location_id")
	var location = _location

	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "filter_id")
	var filter = _filter

	// specifies this is the most recent "new search"
	@DatabaseField(columnName = "recent")
	var recent = 0
	
	def this() = this("", null, null)

	def this(s: String) = this(s, null, null)
	
	override def id = _id
	
	override def value = name
	
	override def toString = name
}