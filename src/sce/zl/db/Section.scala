package sce.zl.db
import java.io.Serializable
import com.j256.ormlite.field.DatabaseField
import sce.zl.traits.Identifiable
import com.j256.ormlite.table.DatabaseTable

@DatabaseTable(tableName = "section")
class Section(_name: String, _code: String) extends Identifiable[Int] with Serializable {
  
  	@DatabaseField(generatedId = true, columnName = "_id")
	private var _id: Int = _

	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "locale_id", index = true)
	var locale: Locale = _

	@DatabaseField(canBeNull = false, columnName = "name")
	var name = _name

	/** 3 letter code for section, or blank */
	@DatabaseField(canBeNull = false, columnName = "code", index = true)
	var code = _code

	@DatabaseField(columnName = "hoods")
	var hoods = false
	
	def this() = this("", "")

	def id = _id
	
	override def value = code.length match {
  	  case 0 => ""
  	  case _ => "/%s" format code
  	}

	override def toString = name
}