package sce.zl.db

import java.io.Serializable
import com.j256.ormlite.field.DatabaseField
import sce.zl.traits.Identifiable

class SubCategory(_name: String, _code: String) extends Identifiable[Int] with Serializable {

  	@DatabaseField(generatedId = true, columnName = "_id")
	private var _id: Int = _

	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "category_id", index = true)
	var category: Category = _

	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "locale_id", index = true)
	var locale: Locale = _
	
	@DatabaseField(canBeNull = false, columnName = "name")
	var name = _name

	/** 3-letter subcategory code */
	@DatabaseField(canBeNull = false, columnName = "code", index = true)
	var code = _code

	def this() = this("", "")
	
	// not used
	override def id = 0

    override def value = "/%s" format code
    
	override def toString = name
}