package sce.zl.traits
import scala.io.Source

import android.app.Activity
import android.os.AsyncTask

// provide a facility to gracefully recover from pause/stop
trait Asyncable extends Alertable { self: Activity =>

  // seems to be easier to call this explicitly to be able to use function literals
  implicit def async[T, P, R](subject: ((P*) => Unit, T*) => R) = new AsyncBuilder[T, P, R](subject)

  // design a dsl for the above class
  protected class AsyncBuilder[T, P, R](subject: ((P*) => Unit, T*) => R) {

    var message = -1
    var prolog: Option[() => Unit] = None
    var epilog: Option[() => Unit] = None
    var predicate: Option[R => Unit] = None
    var caveat: Option[Throwable => Unit] = None
    var progress: Option[(P*) => Unit] = None
    //var publish: (P*) => Unit = _ 

    // this is a very basic internal DSL
    def apply(ps: T*) = execute(ps: _*)
    // include a dialog message
    def inform(m: Int) = { message = m; this }
    // before work
    def pre(p: () => Unit) = { prolog = Option(p); this }
    // during work
    def update(u: (P*) => Unit) = { progress = Option(u); this }
    // after work
    def post(p: () => Unit) = { epilog = Option(p); this }
    // if succeeded
    def then(p: R => Unit) = { predicate = Option(p); this }
    // if failed
    def otherwise(c: Throwable => Unit) = { caveat = Option(c); this }

    // can either invoke directly or via apply (hardwired progress to Int)
    def execute(ts: T*): Unit = new AsyncTask[T, P, Throwable Either R] {

      override def onPreExecute {
        super.onPreExecute

        // execute prolog
        prolog match {
          case Some(x) => x()
          case None => // do nothing
        }

        // handle message
        alert(message)
      }

      override def doInBackground(params: T*) = try Right(subject(publishProgress(_: _*), params: _*)) catch { case e => Left(e) }

      override def onProgressUpdate(status: P*) = progress match {
        case Some(x) => x(status: _*)
        case None => // do nothing
      }

      // perform caveat or predicate
      override def onPostExecute(res: Throwable Either R) {
        super.onPostExecute(res)

        // handle message
        endAlert

        epilog match {
          case Some(x) => x()
          case None => // do nothing
        }

        res.fold(
          t => caveat.toLeft(()).fold(_(t), _ => info("throwable, but no caveat", t)),
          r => predicate.toLeft(()).fold(_(r), _ => info("result, but no predicate")))
      }
    } execute (ts: _*)
  }
}

trait Blockable extends Asyncable with Toastable { self: Activity =>
  def ifNotBlocked[R](f: => R) = async[String, Unit, Boolean] { (publish, params) =>
    val source = Source.fromFile(params(0))
    source.getLines.find(_.contains("admob")) match {
      case Some(x) => { source.close; false }
      case None => { source.close; true }
    }
  } then {
    case true => f
    case false =>
      // detection, close activity
      toastShort("ad blocker detected")
      finish
  } otherwise { e =>
    // failure, close activity
    error("blocker check failed, cannot continue", e)
    finish
  } execute ("/etc/hosts")
}

//// activities which permit flagging of urls
//trait Flaggable extends Asyncable
//  with Parsable
//  with Toastable { self: Activity =>
//  def flag(action: Int) = async[String, Unit, Unit] { (publish, params) =>
//    parseNode((action match {
//      case R.id.ctx_res_best => "%s9"
//      case R.id.ctx_res_misc => "%s16"
//      case R.id.ctx_res_proh => "%s28"
//      case R.id.ctx_res_spam => "%s15"
//    }) format params(0)) { feed => }
//  } inform {
//    R.string.stat_flag
//  }
//}