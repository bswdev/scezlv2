package sce.zl.traits
import android.app.Activity
import sce.zl.db.DatabaseHelper
import com.j256.ormlite.android.apptools.OpenHelperManager

trait Databasable extends Activity
  with Asyncable {

  var helper: Option[DatabaseHelper] = None

  override protected def onDestroy {
    super.onDestroy

    helper match {
      case None => // do nothing
      case Some(x) =>
        OpenHelperManager.releaseHelper
        helper = None
    }
  }

  // curry in database helper and use async functionality
  def executeDb[T, P, R](f: (DatabaseHelper, (P*) => Unit, T*) => R) =
    async[T, P, R](f(getHelper, _, _: _*)) //(Function.uncurried(f.curry(getHelper)))

  protected def getHelper = helper match {
    case Some(x) => x
    case None =>
      helper = Option(OpenHelperManager.getHelper(this, classOf[DatabaseHelper]))
      helper.get
  }
}