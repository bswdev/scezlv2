package sce.zl.traits
import android.app.Activity
import android.os.Handler

// for activities which rather specify a method for handler functionality
trait Handlable extends Handler.Callback { self: Activity =>

  // implement message handling via callback
  val handler = new Handler(this)
}