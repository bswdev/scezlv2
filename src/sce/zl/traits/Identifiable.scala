package sce.zl.traits


// for items displayed in an <code>EventSpinner</code>
trait Identifiable[A] {
  def id: A
  def value: String
}