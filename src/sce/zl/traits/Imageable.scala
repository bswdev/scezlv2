package sce.zl.traits
import java.io._
import java.net.URI
import java.util.Collections

import scala.actors.threadpool.Executors
import scala.collection.JavaConversions.mutableMapAsJavaMap
import scala.collection.mutable.WeakHashMap

import org.apache.http.client.methods.HttpGet
import org.apache.http.entity.BufferedHttpEntity
import org.apache.http.impl.client.DefaultHttpClient

import android.app.Activity
import android.graphics.BitmapFactory.Options
import android.graphics.Bitmap.CompressFormat
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import sce.zl.util._

// for activities which display/manage images
trait Imageable extends Activity
  with Handlable
  with Loggable {

  override def onCreate(state: Bundle) {
    super.onCreate(state)

    // init file cache
    Option(Imageable.fileCache) match {
      case Some(x) => // do nothing

      // initialize file cache
      case None => Imageable.fileCache = new CacheFile(this)
    }
  }

  def clear {
    Imageable.fileCache.clear
    Imageable.memoryCache.clear
  }
  def clearFiles = Imageable.fileCache.clear
  def save(id: String, image: Bitmap) = Imageable.fileCache.put(id, image)

  def display(url: String, image: ImageView, scale: Boolean = false) = Imageable.show(url, image, scale, handler)
}

object Imageable extends Loggable {

  type Photo = Uri Pair Bitmap
  //implicit def func2Run(f: => Unit) = new Runnable { override def run = f }

  val msgShow = 60
  val msgHide = 61
  val poolSize = 2
  val bufferSize = 1 << 16
  val reqSize = 84

  private var fileCache: CacheFile = _
  lazy private val memoryCache = new CacheMemory[Photo](500)
  lazy private val pool = Executors.newFixedThreadPool(poolSize)

  // image tracker, views to urls (synchronized)
  lazy val loading = Collections.synchronizedMap(new WeakHashMap[ImageView, String])

  def show(url: String, image: ImageView, scale: Boolean, handler: Handler) {
    // thumb key
    val key = url + (if (scale) "t" else "")

    memoryCache.get(key) match {
      // load image from memory  
      case Some(x) =>
        // send image to calling activity
        //val msg = handler.obtainMessage
        //			// set key on tag
        //			image.setTag(key);
        //msg.what = msgShow
        //msg.arg1 = image.getId
        //msg.obj = x
        //msg.sendToTarget
        image.setImageBitmap(x._2)

        //			// set key on tag
        //			image.setTag(key);

        // set image visible
        image.setVisibility(View.VISIBLE)

      // load image from web/file
      case None =>
        // store view
        loading.put(image, key)

        // hide image
        image.setVisibility(View.GONE)

        // queue photo to load
        pool.execute(new Loader(url, image, handler, scale))
    }
  }

  class Loader(url: String, image: ImageView, handler: Handler, scale: Boolean) extends Runnable {

    lazy val key = url + (if (scale) "t" else "")

    override def run {
      def expired = !loading.get(image).equals(key)

      // check if view is still valid
      if (expired) return

      // fetch the photo (from file or web) & store in memory
      load.fold(
        warn("could not load photo %s" format url, _),
        photo => {
          // store in memory
          memoryCache.put(key, photo)

          // check if view is still valid
          if (expired) return

          // check for scale/thumb, if scaled render directly to image view (i.e., recyclable views)
          scale match {
            case true =>
              // post to handler
              handler.post(new Runnable {
                override def run {
                  // check if image is still valid
                  if (expired) return

                  // set image
                  image.setImageBitmap(photo._2)
                  image.setVisibility(View.VISIBLE)
                }
              })
            case false =>
              // send photo to calling activity
              val msg = handler.obtainMessage
              msg.what = msgShow
              msg.arg1 = image.getId
              msg.obj = photo
              msg.sendToTarget
          }
        })
    }

    def decode(file: File): Throwable Either Bitmap = try decode(new FileInputStream(file)) catch { case e => Left(e) }

    def decode(stream: InputStream): Throwable Either Bitmap = try {
      // create buffered stream
      // sometimes throws out-of-memory exception on "init"
      val bis = new BufferedInputStream(stream, Imageable.bufferSize)

      // decode image size
      var options = new Options
      var exp: Int = 0

      // determine the sample exponent
      if (scale) {
        // shrink down to required size
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(bis, null, options)

        // calculate scale; it should be a power of 2
        val width: Double = options.outWidth
        val height: Double = options.outHeight

        // image exceeds in both dimensions; can scale down by at
        // least half
        if (height > Imageable.reqSize && width > Imageable.reqSize) {
          // log(REQ_SIZE/min-dim) / log (0.5) ... dim * 0.5^x = REQ_SIZE,
          // solving for x
          val x = Math.log(Imageable.reqSize / Math.min(height, width)) / Math.log(0.5)
          exp = Math.round(x.toFloat)
        }

        bis.reset
      }

      // decode with inSampleSize
      options = new Options
      options.inSampleSize = 1 << exp

      val bm = BitmapFactory.decodeStream(bis, null, options)

      // important to add!
      bis.close

      Right(bm)
    } catch {
      case e => Left(e)
    }

    def load: Throwable Either Photo = {
      // fetch the file
      val file = fileCache.get(key)
      val uri = Uri.fromFile(file)

      // from SD cache (maybe out of memory cache, but still in file cache)
      decode(file).fold(
        // just issue a warning and continue
        warn("file not found", _),
        bitmap => return Right(uri -> bitmap))

      // from web
      try {
        val client = new DefaultHttpClient

        // sometimes throws an out-of-memory exception on "expand"
        val stream = new BufferedHttpEntity(client
          .execute(new HttpGet(URI.create(url))).getEntity).getContent

        decode(stream).fold(
          t => {
            // close connections
            stream.close
            client.getConnectionManager.shutdown

            // warn user of issue
            warn("could not decode stream", t)
            Left(t)
          },
          bitmap => {
            // close connections
            stream.close
            client.getConnectionManager.shutdown

            // just write the smaller version
            val fos = new FileOutputStream(file)
            bitmap.compress(CompressFormat.JPEG, 100, fos)
            fos.close
            Right(uri -> bitmap)
          })
      } catch {
        case e => Left(e)
      }
    }
  }
}