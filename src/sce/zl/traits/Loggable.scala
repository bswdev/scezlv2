package sce.zl.traits
import android.app.Activity
import android.util.Log
import android.widget.Toast
import org.holoeverywhere.app.Dialog
import org.holoeverywhere.app.ProgressDialog

// logging facilities
trait Loggable {

  val tag = "sce.zl"

  def verbose(msg: String) = Log.v(tag, msg)
  def verbose(msg: String, t: Throwable) = Log.v(tag, msg, t)
  def debug(msg: String) = Log.d(tag, msg)
  def debug(msg: String, t: Throwable) = Log.d(tag, msg, t)
  def info(msg: String) = Log.i(tag, msg)
  def info(msg: String, t: Throwable) = Log.i(tag, msg, t)
  def warn(msg: String) = Log.w(tag, msg)
  def warn(msg: String, t: Throwable) = Log.w(tag, msg, t)
  def error(msg: String) = Log.e(tag, msg)
  def error(msg: String, t: Throwable) = Log.e(tag, msg, t)
}

// toasting
trait Toastable { self: Activity =>
  def toastShort(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show
  def toastShort(msg: Int) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show
  def toastLong(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_LONG).show
  def toastLong(msg: Int) = Toast.makeText(this, msg, Toast.LENGTH_LONG).show
}

// for tasks which use progress dialogs
trait Alertable extends Loggable { self: Activity =>

  var dialog: Option[Dialog] = None

  def alert(msg: Int) = msg match {
    case -1 => // do nothing
    case m => try {
      dialog = Option(ProgressDialog.show(this, null, getString(m), true, true))
    } catch {
      case e =>
        dialog = None
        warn("owning activity stopped", e)
    }
  }

  def endAlert = dialog match {
    case Some(d) =>
      d.dismiss
      dialog = None
    case None => // ignored
  }
}