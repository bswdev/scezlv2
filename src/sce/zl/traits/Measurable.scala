package sce.zl.traits
import android.app.Activity
import android.util.DisplayMetrics
import android.util.Log
import android.os.Bundle
import android.content.res.Configuration

trait Measurable extends Activity
	with Loggable {

  override def onCreate(state: Bundle) {
    super.onCreate(state)
    getWindowManager.getDefaultDisplay.getMetrics(Measurable.metrics)
    debug("calculating metrics")
  }

  override def onConfigurationChanged(config: Configuration) {
    super.onConfigurationChanged(config)
    getWindowManager.getDefaultDisplay.getMetrics(Measurable.metrics)
  }

  def measuredWidth = Measurable.metrics.widthPixels
  def measuredHeight = Measurable.metrics.heightPixels
}

object Measurable {

  lazy val metrics = new DisplayMetrics
}