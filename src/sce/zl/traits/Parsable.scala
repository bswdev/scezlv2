package sce.zl.traits
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl
import org.xml.sax.InputSource
import scala.xml.Node
import scala.xml.parsing.NoBindingFactoryAdapter
import javax.net.ssl.SSLContext
import org.jsoup.Jsoup
import javax.net.ssl.HttpsURLConnection
import org.jsoup.nodes.Document
import javax.net.ssl.X509TrustManager
import java.net.URL
import org.apache.http.params.BasicHttpParams
import org.apache.http.params.HttpProtocolParams
import org.apache.http.HttpVersion
import org.apache.http.protocol.HTTP
import org.apache.http.impl.client.DefaultHttpClient
import java.io.InputStream
import org.apache.http.client.methods.HttpGet
import org.apache.http.conn.ClientConnectionManager
import java.security.KeyStore
import org.apache.http.params.HttpParams
import org.apache.http.conn.scheme.SchemeRegistry
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.scheme.PlainSocketFactory
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
import javax.security.cert.X509Certificate
import java.net.Socket
import java.security.cert.X509Certificate

// for parsing xml/html input streams
trait Parsable {

  def parseNode[A](url: String)(f: Node => A): A = {
    val parser = Parsable.parserFactory.newSAXParser
    val source = new InputSource(url)
    val adapter = new NoBindingFactoryAdapter
    f(adapter.loadXML(source, parser))
  }

  def parseDoc[A](url: String)(f: Document => A): A = {
    val get = new HttpGet(url)
    val is = Parsable.client.execute(get).getEntity.getContent
    //Parsable.client.getCookieStore.getCookies
    //val is = new URL(url).openStream
    f(Jsoup.parse(is, null, url))
  }
}

class ParseSocketFactory(store: KeyStore) extends SSLSocketFactory(store) {
  val ctx = SSLContext.getInstance("TLS")

  // ignore ssl certs
  ctx.init(
    null,
    Array(new X509TrustManager {
      override def checkClientTrusted(certs: Array[java.security.cert.X509Certificate], authType: String){}
      override def checkServerTrusted(certs: Array[java.security.cert.X509Certificate], authType: String){}
      override def getAcceptedIssuers(): Array[java.security.cert.X509Certificate] = Array[java.security.cert.X509Certificate]()
    }),
    null)

  override def createSocket(socket: Socket, host: String, port: Int, autoClose: Boolean) =
    ctx.getSocketFactory.createSocket(socket, host, port,
      autoClose)
  override def createSocket = ctx.getSocketFactory.createSocket
}

object Parsable {

  val parserFactory = new SAXFactoryImpl

  private val trustStore = KeyStore.getInstance(KeyStore.getDefaultType)
  trustStore.load(null, null)

  private val sf = new ParseSocketFactory(trustStore)
  sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)

  private val registry = new SchemeRegistry
  registry register new Scheme("http", PlainSocketFactory.getSocketFactory, 80)
  registry register new Scheme("https", sf, 443)

  private val params = new BasicHttpParams
  HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1)
  HttpProtocolParams.setContentCharset(params, HTTP.UTF_8)

  val client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params)
} 