package sce.zl.traits

import android.app.Activity
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

// for activities which execute work on a thread pool
trait Poolable extends Activity
  with Loggable {

  override protected def onPause {
    // terminate/shutdown pool
    Poolable.pool match {
      case Some(x) =>
        x.shutdown
        Poolable.pool = None
      case None => // do nothing
    }

    super.onPause
  }

  override protected def onResume {
    super.onResume

    // create new pool
    openPool
  }

  def queue(task: Runnable, failure: => Unit): Unit = Poolable.pool match {
    // execute
    case Some(x) => try x.execute(task)
    catch {
      case e =>
        error("could not submit task to pool", e)
        failure
    }

    // create pool and execute
    case None =>
      openPool
      queue(task, failure)
  }

  private def openPool = Poolable.pool = Option(Executors.newFixedThreadPool(Poolable.poolSize))
}

object Poolable {

  val poolSize = 2 // max threads in pool

  var pool: Option[ExecutorService] = None
}