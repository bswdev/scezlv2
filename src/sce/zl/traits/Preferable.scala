package sce.zl.traits
import java.io.Serializable

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager

// for activities which maintain preferences
trait Preferable extends Activity with Loggable {

  override protected def onCreate(state: Bundle) {
    super.onCreate(state)

    // init preferences
    Option(Preferable.preferences) match {
      case Some(x) => // do nothing
      case None =>
        info("initializing preferences")
        Preferable.preferences = PreferenceManager.getDefaultSharedPreferences(this)
    }
  }

  def fromPref[A](id: String, default: A) = (default match {
    case i: Int => Preferable.preferences.getInt(id, i)
    case s: String => Preferable.preferences.getString(id, s)
    case b: Boolean => Preferable.preferences.getBoolean(id, b)
  }).asInstanceOf[A]

  def toPref[A](id: String, value: A) = value match {
    case i: Int => Preferable.preferences.edit.putInt(id, i).commit
    case s: String => Preferable.preferences.edit.putString(id, s).commit
    case b: Boolean => Preferable.preferences.edit.putBoolean(id, b).commit
  }
}

object Preferable {

  private var preferences: SharedPreferences = _
}

// for activities which save/restore state
trait Restorable { self: Activity =>

  def toState[A](id: String, item: A)(implicit state: Bundle) = item match {
    case i: Int => state.putInt(id, i)
    case s: String => state.putString(id, s)
    case b: Boolean => state.putBoolean(id, b)
    case x if Option(x).isDefined => state.putSerializable(id, x.asInstanceOf[Serializable])
    case _ => // null, do nothing
  }

  // pattern match on erasure of function argument
  // can't use implicits and context bounds together
  def fromState[A: Manifest](state: Bundle, id: String)(fa: A => Unit) = manifest[A].erasure match {
    case c if c == classOf[Int] => fa(state.getInt(id).asInstanceOf[A])
    case c if c == classOf[String] => fa(state.getString(id).asInstanceOf[A])
    case c if c == classOf[Boolean] => fa(state.getBoolean(id).asInstanceOf[A])
    case _ => fa(state.getSerializable(id).asInstanceOf[A])
  }

  //    fa match {
  //    case i: (Int => Unit) => fa(state.getInt(id).asInstanceOf[A])
  //    case s: (String => Unit) => fa(state.getString(id).asInstanceOf[A])
  //    case b: (Boolean => Unit) => fa(state.getBoolean(id).asInstanceOf[A])
  //    case x => fa(state.getSerializable(id).asInstanceOf[A])
  //  }
}

trait Intentable { self: Activity =>

  lazy val intent = getIntent

  // pattern match on erasure of function argument
  def fromIntent[A: Manifest](id: String)(fa: A => Unit) = manifest[A].erasure match {
    case c if c == classOf[Int] => fa(intent.getIntExtra(id, 0).asInstanceOf[A])
    case c if c == classOf[String] => fa(intent.getStringExtra(id).asInstanceOf[A])
    case c if c == classOf[Boolean] => fa(intent.getBooleanExtra(id, false).asInstanceOf[A])
    case _ => fa(intent.getSerializableExtra(id).asInstanceOf[A])
  }

  def toIntent[A](id: String, item: A)(implicit intent: Intent) = item match {
    case i: Int => intent.putExtra(id, i)
    case s: String => intent.putExtra(id, s)
    case b: Boolean => intent.putExtra(id, b)
    case x if Option(x).isDefined => intent.putExtra(id, x.asInstanceOf[Serializable])
    case _ => // do nothing
  }

  //  fa match {
  //    case i: (Int => Unit) => fa(intent.getIntExtra(id, 0).asInstanceOf[A])
  //    case s: (String => Unit) => fa(intent.getStringExtra(id).asInstanceOf[A])
  //    case b: (Boolean => Unit) => fa(intent.getBooleanExtra(id, false).asInstanceOf[A])
  //    case x => fa(intent.getSerializableExtra(id).asInstanceOf[A])
  //  }
}