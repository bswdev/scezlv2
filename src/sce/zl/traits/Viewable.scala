package sce.zl.traits

import android.app.Activity
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.View
import android.content.DialogInterface

// for activity/widget which needs implicit conversions from int to views, closures to listeners
trait Viewable { self: { def findViewById(id: Int): View } =>

  def view[A <: View](id: Int, view: View): A = view.findViewById(id).asInstanceOf[A]
  
  // int/id to view
  implicit def int2View[A <: View](id: Int) /*(implicit ev: A <:< View)*/ : A = findViewById(id).asInstanceOf[A]

  // func to View.OnClickListener
  implicit def func2ClickListener(f: View => Unit): View.OnClickListener = new View.OnClickListener {
    override def onClick(v: View) = f(v)
  }

  implicit def func2DialogClickListener(f: (DialogInterface, Int) => Unit) = new DialogInterface.OnClickListener {
    override def onClick(dialog: DialogInterface, which: Int) = f(dialog, which)
  }

  implicit def func2MultiChoiceDialogClickListener(f: (DialogInterface, Int, Boolean) => Unit) =
    new DialogInterface.OnMultiChoiceClickListener {
      override def onClick(dialog: DialogInterface, which: Int, checked: Boolean) = f(dialog, which, checked)
    }

  implicit def func2OnCancelListener(f: DialogInterface => Unit) = new DialogInterface.OnCancelListener {
    override def onCancel(dialog: DialogInterface) = f(dialog)
  }
  //implicit def func2SpinnerSelectListener[T](f: (T, Int) => Unit) = new Spinner
}

/**
 * Did not use implicit conversions because type restrictions can only be enforced on trait/class "type parameters"
 * (and method params) and leads to ambiguity when traits mixed in conjunction.
 */
trait Animatable { self: Activity =>

  // int/id to animation
  def anim(id: Int): Animation = AnimationUtils.loadAnimation(this, id)
}

//// for activities which use the custom title bar
//trait Titleable extends Activity
//  with Viewable {
//
//  val titleLayout: Int
//
//  lazy val _title: TextView = android.R.id.title
//
//  private var customTitle: Boolean = _
//
//  override def onCreate(state: Bundle) {
//    // change the theme from no title bar to custom (this is done to
//    // prevent a title from being shown while activity is still rendering)
//    setTheme(R.style.theme_ezlist)
//
//    // request before setting content
//    customTitle = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE)
//
//    super.onCreate(state)
//  }
//
//  override def setContentView(layout: Int) {
//    super.setContentView(layout)
//
//    // set custom title if it is supported
//    customTitle match {
//      case true => getWindow.setFeatureInt(Window.FEATURE_CUSTOM_TITLE, titleLayout)
//      case false => // do nothing
//    }
//  }
//
//  override def setTitle(title: CharSequence) {
//    super.setTitle(title)
//    _title.setText(title)
//  }
//
//  override def setTitle(titleId: Int) {
//    super.setTitle(titleId)
//    _title.setText(titleId)
//  }
//}