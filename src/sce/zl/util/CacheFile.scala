package sce.zl.util
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import java.net.URLEncoder

class CacheFile(context: Context) {

  // root path
  val directory = "com.scezl"

  // root cache directory
  var root: File = _

  // Find the dir to save cached images
  if (Environment.getExternalStorageState.equals(Environment.MEDIA_MOUNTED))
    root = new File(Environment.getExternalStorageDirectory, directory)
  else
    root = context.getCacheDir

  // create the root
  if (!root.exists)
    root.mkdirs

  def get(id: String): File = new File(root, URLEncoder.encode(id.replaceAll("\\*", "")))

  /**
   * Save an image to save directory.
   *
   * @param url
   * @param image
   * @returns file path where image is saved
   */
  def put(id: String, image: Bitmap): String = {
    // create saved directory if it does not exist
    val save = new File(root, "saved")
    if (!save.exists)
      save.mkdir

    // create the save image
    val fos = new FileOutputStream(new File(save, URLEncoder.encode(id.replaceAll("\\*", ""))));
    image.compress(CompressFormat.JPEG, 100, fos)
    fos.close
    "saved to: " + save.getAbsolutePath
  }

  def clear = try {
    root.listFiles.foreach(_.delete)
  } catch {
    case e =>
    // ignored, user may have forcibly deleted directory
  }
}