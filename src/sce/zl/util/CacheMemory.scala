package sce.zl.util
import java.util.Collections
import java.util.LinkedHashMap
import scala.collection.JavaConversions._
import scala.ref.SoftReference
import java.util.Map

/**
 * Cache implemented in memory.
 */
class CacheMemory[A <: AnyRef](val size: Int) {

  // starting size
  val initSize = 20

  // images LRU cache
  private val lru: scala.collection.mutable.Map[String, SoftReference[A]] =
    Collections.synchronizedMap(new LinkedHashMap[String, SoftReference[A]](initSize, 0.75f, true) {
      override def removeEldestEntry(entry: Map.Entry[String, SoftReference[A]]) = size > CacheMemory.this.size
    })

  def get(id: String): Option[A] = for (
    o <- lru.get(id);
    a <- o.get
  ) yield a

  def put(id: String, a: A): Unit = lru += id -> new SoftReference(a)

  // clear
  def clear = lru.clear
}