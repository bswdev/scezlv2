package sce.zl.widget

import android.widget.ArrayAdapter
import android.content.Context
import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import sce.zl.traits.Identifiable

class BaseSpinnerAdapter[T <: Identifiable[Int]](context: Context, listItemResourceId: Int, _items: ListBuffer[T] = ListBuffer[T]())
  extends ArrayAdapter[T](context, listItemResourceId, _items) {

  def retainData(items: List[T]) {
    _items.retainAll(items)
    notifyDataSetChanged
  }

  def setData(items: List[T]) {
    _items.clear
    //clear();
    //System.gc();
    _items.addAll(items)
    notifyDataSetChanged
  }

  def reset = setData(Nil)
  
  override def getItemId(position: Int) = _items(position).id
}