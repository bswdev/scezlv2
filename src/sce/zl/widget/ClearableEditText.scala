package sce.zl.widget

import android.widget.RelativeLayout
import android.view.ViewParent
import android.util.AttributeSet
import android.content.Context
import android.view.LayoutInflater
import sce.zl.traits._
import sce.zl.R
import android.text.TextWatcher
import android.widget.Button
import android.view.View.OnClickListener
import android.view.View
import android.text.Editable
import org.holoeverywhere.widget.EditText

class ClearableEditText(context: Context, attrs: AttributeSet)
  extends RelativeLayout(context, attrs)
  with Viewable {

  // layout
  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE).asInstanceOf[LayoutInflater]
    .inflate(R.layout.wgt_clear_edit, this, true)

  // widgets (not sure why viewable does not work here)
  val text: EditText = findViewById(R.id.edit).asInstanceOf[EditText]
  
  val clear: Button = findViewById(R.id.clear).asInstanceOf[Button]
  
  // styles
  val typedArray = context.obtainStyledAttributes(attrs, R.styleable.sce_zl_widget_ClearableEditText)

  // text style
  text.setTextAppearance(
    context,
    typedArray.getResourceId(
      R.styleable.sce_zl_widget_ClearableEditText_textStyle,
      android.R.style.TextAppearance_Medium))

  // text hint
  text.setHint(typedArray.getString(R.styleable.sce_zl_widget_ClearableEditText_android_hint))

  // don't forget this!
  typedArray.recycle

  // listeners
  
  // clear listener
  clear.setOnClickListener((v: View) => text.setText(""))

  // toggle listener
  text.addTextChangedListener(new TextWatcher() {
    override def onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) =
      clear.setVisibility(if (s.length > 0) View.VISIBLE else View.GONE)
    override def beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override def afterTextChanged(editable: Editable) {}
  })

  def getText: CharSequence = text.getText

  def setSelection(index: Int) = text.setSelection(index)

  def setText(text: CharSequence) = this.text.setText(text)
}