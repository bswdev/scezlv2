package sce.zl.widget

import android.content.Context
import android.util.AttributeSet
import java.lang.reflect.Method
import android.widget.AdapterView
import sce.zl.traits._
import org.holoeverywhere.widget.Spinner

class EventSpinner(context: Context, attrs: AttributeSet) extends Spinner(context, attrs)
  with Loggable {

  def getAs[T](clazz: Class[T]): T = try {
    // sometimes get error here from index == length
    getSelectedItem.asInstanceOf[T]
  } catch {
    case e =>
      // fix for ioobe in all versions, return last item if error
      getItemAtPosition(getCount - 1).asInstanceOf[T]
  }

  /**
   * Retrieve the value of the selected item.
   */
  def getSelectedItemValue: String = Option(getAs(classOf[Identifiable[_]])) match {
    case Some(x) => x.value
    case None => ""
  }

  def testReflectionForSelectionChanged = EventSpinner.s_pSelectionChangedMethod match {
    case Some(x) => try x.invoke(this, Array[Object](): _*) catch { case e => error("custom spinner reflection bug", e) }
    case None => error("no selection method available")
  }

  // fix for ioobe in 0.7.10
  override def setSelection(position: Int, animate: Boolean) = if (getCount != 0) {
    // get current position
    val curpos = getSelectedItemPosition

    // set new position (call super)
    super.setSelection(position, animate);

    // make sure that the change method (i.e., item selected) is fired even
    // on duplicated selections
    if (animate && curpos == position)
      testReflectionForSelectionChanged
  }

  /**
   * Sets the item with the given <code>id</code> as selected.
   *
   * @param id
   * @param animate
   *            also used to specify if event should be fired even if
   *            selection hasn't changed
   */
  def setSelectedItemPositionWithId(id: Long) = (0 until getCount).find(getItemIdAtPosition(_) == id) match {
    case Some(x) => setSelection(x, true)
    case None => // do nothing
  }

  /**
   * Sets the item with the given <code>value</code> as selected.
   *
   * @param value
   */
  def setSelectedItemPositionWithValue(value: String) =
    // compare to identifiable
    (0 until getCount).find(value == getItemAtPosition(_).asInstanceOf[Identifiable[_]].value) match {
      case Some(x) => setSelection(x, true)
      case None => // do nothing
    }
}

object EventSpinner extends Loggable {

  lazy val s_pSelectionChangedMethod: Option[Method] = {
    Option(classOf[AdapterView[_]].getDeclaredMethod("selectionChanged", Array[Class[_]](): _*)) match {
      case Some(x) => { x.setAccessible(true); Some(x) }
      case None => None
    }
  }

  //  try {
  //    //val noparams: Array[java.lang.Class[_]] = Array[java.lang.Class[_]]()
  //    val targetClass: Class[_] = classOf[AdapterView[_]]
  //    //s_pSelectionChangedMethod = targetClass.getDeclaredMethod("selectionChanged")
  //    Option(targetClass.getDeclaredMethod("selectionChanged")) match {
  //      case Some(x) => x.setAccessible(true)
  //      case None => // do nothing
  //    }
  //    //			if (s_pSelectionChangedMethod != null) {
  //    //				s_pSelectionChangedMethod.setAccessible(true);
  //    //			}
  //  } catch {
  //    case e =>
  //      error("Custom spinner, reflection bug: ", e)
  //      //Log.e("Custom spinner, reflection bug:", e.getMessage());
  //      throw new RuntimeException(e)
  //  }
}