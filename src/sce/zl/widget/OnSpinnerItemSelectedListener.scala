package sce.zl.widget

import android.view.View
import org.holoeverywhere.widget.AdapterView.OnItemSelectedListener
import org.holoeverywhere.widget.AdapterView


abstract class OnSpinnerItemSelectedListener[T] extends OnItemSelectedListener {

  override def onItemSelected(parent: AdapterView[_], view: View, position: Int, id: Long): Unit =
    onSpinnerItemSelected(parent.getItemAtPosition(position).asInstanceOf[T], position)

  override def onNothingSelected(parent: AdapterView[_]) {}

  /**
   * Handle the selected item.
   *
   * @param item
   */
  def onSpinnerItemSelected(item: T, position: Int)
}